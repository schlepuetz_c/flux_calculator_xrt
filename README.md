# TOMCAT flux calculator

Welcome to the git repository of the beamline flux and spectrum calculator notebook.

# How to run the calculator
The calculator is written as a python Jupyter notebook. There are several ways
you can run the calculator, either by installing a proper python environment
locally or by making use of the binder service (https://mybinder.org/) to run
the notebook on a remote service in the browser window.

## Option 1: Running the calculator in your browser

**NOTE:** 
This does not seem to work for most calculations! Apparently, the source calculations are too resource-intensive to run on the free and shared binder infrastructure... It might work for calculations with small mesh sizes (bending magnet)

Click the below Binder image or link to launch a Jupyter Lab with access to the calculator
without the need to download or install anything locally on your machine.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.psi.ch%2Fschlepuetz_c%2Fflux_calculator_xrt/HEAD)

Link: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.psi.ch%2Fschlepuetz_c%2Fflux_calculator_xrt/HEAD

Note that it will take a few minutes to set up the remote python environment,
so please be patient.

The simply click on the "Flux_calculator_xrt.ipynb" file in the left panel to open it in the main window.


## Option 2: Running the notebook locally

In order to run the notebook locally on your computer, you need to set up a
python environment with the necessary packages. At the moment, these include

* python=3
* ipywidgets
* matplotlib
* numpy
* pandas
* scipy
* xraydb >= 4.0.0
* xrt >= 1.6

Optionally, the following packages are required when wanting to save output from
the calculations:

* pickle
* pillow

The repository contains an environment file with which you can directly install
the environment using conda (see below)

### Download the calculator

Download the calculator either by cloning the git repository (requires a local git installation)

```bash
git clone https://gitlab.psi.ch/schlepuetz_c/flux_calculator_xrt.git
```

or by downloading the zip file with the latest version:

https://gitlab.psi.ch/schlepuetz_c/flux_calculator_xrt/-/archive/main/flux_calculator_xrt-main.zip


### Installing conda

The easiest way to create a dedicated python environment is by installing
Anaconda (https://anaconda.org/) or miniconda
(https://docs.conda.io/en/latest/miniconda.html) on your system. This provides
you with the conda environment management system.

### Install and activate the calculator environment
This step is optional, but if you like, you can install a dedicated conda
environment for this calculator that comes with the correct packages already
configured. If you have another environment that already meets the above listed
requirements, then you can use that environment instead.

#### Using the command line

**NOTE for Mac users (Apple Silicon M-Series Processors):**

Read on below, an additional installation step is required!

For all other systems, do the following:

Locate the `environment.yml` file in this repository and run the following
command:

```bash
conda env create -f environment.yml
```
This will create a new environment called `flux_calculator_xrt`. Once the
installation has finished, you then need to activate this environment:

```bash
conda activate flux_calculator_xrt
```

**Installing the environment on a Mac with Apple Silicon M-Series processor**:

Installing the environment on a Mac with the new Apple Silicon M# processor
family is a bit more tricky as the necessary version of the xrt library is not
available to run on this architecture natively (yet?). Instead, we need to make
sure that the intel-based packages are used (running in Rosetta). To do so,
specify the architecture before the installation command from the yaml file via
the environment variable `CONDA_SUBDIR` (note that this must be in the same
command):

```CONDA_SUBDIR=osx-64 conda env create -f environment.yml```

After creating this environment, it is still necessary to set the configuration
for that environment to use the osx-64 architecture permanently:

```bash
conda activate flux_calculator_xrt
conda config --env --set subdir osx-64
```

The latter step of updating the config is only required once. From then on, you
can activate the environment as usual:

```bash
conda activate flux_calculator_xrt
```

#### Using Anaconda-Navigator

**NOTE for Mac users (Apple Silicon M-Series Processors):**

You need to use the manual install via the command line for the time being,
read above for instructions.

When using the Anaconda-Navigator, you can also install the new environment via
the GUI interface. Open the Anaconda-Navigator application, then select the
*Environments* tab in the left navigation bar. Click on the *Import* button
at the bottom of the environments list.

In the dialog window, navigate to the `environment.yml` file. The name of the
environment will be inserted automatically. The click *Import* and wait for
the installation to finish.

Now you can always activate this environment by selecting it in the
environments list.


### Start the jupyter lab server and open the notebook

In the python environment you would like to use to run the calculator, you now
need to start the jupyter lab server:

```bash
jupyter lab
```

If using the Anaconda-Navigator, you can also click on the *play* symbol next
to the calculator environment and select *Open with Jupyter Notebook*.

Now either a browser will open automatically with the correct window, or you
have to copy and past the generated html link to your browser to open the
jupyter notebook service.
